﻿using IdentityServer.Infrastructure.Migrations;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Security.Claims;

namespace IdentityServer.Infrastructure.SeedData
{
    public class AspIdentitySeedData
    {
        public static void EnsureSeedData(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                context.Database.Migrate();
                #region Role
                string[] roles = new string[] { "Admin", "User" };

                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                var admin = roleManager.FindByNameAsync("Admin").Result;
                if (admin == null)
                {
                    var role = new IdentityRole { Name = "Admin" };
                    var result = roleManager.CreateAsync(role).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var claim = new Claim("admin_code", "123456");
                    result = roleManager.AddClaimAsync(role, claim).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                }

                Console.WriteLine("Role Admin created");

                var user = roleManager.FindByNameAsync("User").Result;
                if (user == null)
                {
                    var role = new IdentityRole { Name = "User" };
                    var result = roleManager.CreateAsync(role).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var claim = new Claim("user_code", "654321");
                    result = roleManager.AddClaimAsync(role, claim).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                }

                Console.WriteLine("Role User created");
                #endregion

                #region User and User Role
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var loki = userManager.FindByNameAsync("Loki").Result;
                if (loki == null)
                {
                    loki = new ApplicationUser
                    {
                        UserName = "Loki",
                        Email = "loki@upheads.no"
                    };
                    var result = userManager.CreateAsync(loki, "Abc123@").Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var test = userManager.AddToRoleAsync(loki, "Admin").Result;

                    result = userManager.AddClaimsAsync(loki, new Claim[]{
                        new Claim("given_name", "Loki"),
                        new Claim("family_name", "Loki"),
                        new Claim("address", "1, Main Road"),
                        new Claim("role", "Admin"),
                        new Claim("level", "FreeAccount"),
                        new Claim("country", "no")
                    }).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                    Console.WriteLine("loki created");
                }
                else
                {
                    Console.WriteLine("loki already exists");
                }

                var annie = userManager.FindByNameAsync("Annie").Result;
                if (annie == null)
                {
                    annie = new ApplicationUser
                    {
                        UserName = "Annie",
                        Email = "annie@upheads.no"
                    };
                    var result = userManager.CreateAsync(annie, "Abc123@").Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var test = userManager.AddToRoleAsync(annie, "Admin").Result;

                    result = userManager.AddClaimsAsync(annie, new Claim[]{
                        new Claim("given_name", "annie"),
                        new Claim("family_name", "annie"),
                        new Claim("address", "2, Big Street"),
                        new Claim("role", "Admin"),
                        new Claim("level", "PaidAccount"),
                        new Claim("country", "vn")
                    }).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                    Console.WriteLine("Annie created");
                }
                else
                {
                    Console.WriteLine("Annie already exists");
                }

                var kenny = userManager.FindByNameAsync("Kenny").Result;
                if (kenny == null)
                {
                    kenny = new ApplicationUser
                    {
                        UserName = "Kenny",
                        Email = "kenny@upheads.no"
                    };
                    var result = userManager.CreateAsync(kenny, "Abc123@").Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    var test = userManager.AddToRoleAsync(kenny, "User").Result;

                    result = userManager.AddClaimsAsync(kenny, new Claim[]{
                        new Claim("given_name", "Kenny"),
                        new Claim("family_name", "Kenny"),
                        new Claim("address", "2, Big Street"),
                        new Claim("role", "User"),
                        new Claim("level", "PaidAccount"),
                        new Claim("country", "vn")
                    }).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                    Console.WriteLine("Kenny created");
                }
                else
                {
                    Console.WriteLine("Kenny already exists");
                }
                #endregion
            }
        }
    }
}
