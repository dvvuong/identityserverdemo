﻿var extractToken = function (callbackUrl) {
    var returnValue = callbackUrl.split('#')[1];
    var values = returnValue.split('&');

    for (var i = 0; i < values.length; i++) {
        var v = values[i];
        var pairValue = v.split('=');
        localStorage.setItem(pairValue[0], pairValue[1]);
    }

    //window.location.href = '/home/index';
}

extractToken(window.location.href);