﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MvcClient3.Services
{
    public interface IHttpClientService
    {
        Task<HttpClient> GetClient();
        Task<string> RefreshTokens();
        HttpClient CreateClient(string accessToken);
    }
}
