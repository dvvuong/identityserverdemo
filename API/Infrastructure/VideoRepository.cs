﻿using API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Infrastructure
{
    public class VideoRepository : IVideoRepository
    {
        VideoContext _context;

        public VideoRepository(VideoContext videoContext)
        {
            _context = videoContext;
        }

        public void AddVideo(Video video)
        {
            _context.Videos.Add(video);
        }

        public void DeleteVideo(Video video)
        {
            _context.Videos.Remove(video);
        }

        public IEnumerable<Video> GetVideosByOwner(string ownerId)
        {
            return _context.Videos.Where(v => v.OwnerId == ownerId).OrderBy(v => v.Title).ToList();
        }

        public IEnumerable<Video> GetVideos()
        {
            return _context.Videos.ToList();
        }

        public Video GetVideo(Guid id)
        {
            return _context.Videos.FirstOrDefault(v => v.Id == id);
        }

        public bool VideoExists(Guid id)
        {
            return _context.Videos.Any(v => v.Id == id);
        }

        public bool IsVideoOwner(Guid id, string ownerId)
        {
            return _context.Videos.Any(i => i.Id == id && i.OwnerId == ownerId);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }

        public void UpdateVideo(Video video)
        {
            throw new NotImplementedException();
        }

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //protected virtual void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        if (_context != null)
        //        {
        //            _context.Dispose();
        //            _context = null;
        //        }

        //    }
        //}
    }
}
