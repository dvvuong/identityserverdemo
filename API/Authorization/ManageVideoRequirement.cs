﻿using Microsoft.AspNetCore.Authorization;

namespace API.Authorization
{
    public class ManageVideoRequirement : IAuthorizationRequirement
    {
        public ManageVideoRequirement(params string[] inRoles)
        {
            RequiredRoles = inRoles ?? new string[0];
        }

        public string[] RequiredRoles { get; }
    }
}
