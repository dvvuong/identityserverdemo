﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model;
using MvcClient4.Models;
using MvcClient4.Services;
using Newtonsoft.Json;

namespace MvcClient4.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpClientService _httpClientService;

        public HomeController(ILogger<HomeController> logger, IHttpClientService httpClientService)
        {
            _logger = logger;
            _httpClientService = httpClientService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public async Task<ActionResult> ExchangeToken()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");
            var idToken = await HttpContext.GetTokenAsync("id_token");
            //var refreshToken = await HttpContext.GetTokenAsync("refresh_token");

            var accessTokenObj = new JwtSecurityTokenHandler().ReadJwtToken(accessToken);
            var idTokenObj = new JwtSecurityTokenHandler().ReadJwtToken(idToken);

            var clients = _httpClientService.CreateClient(accessToken);
            var response = await clients.GetAsync("api/videos");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var result = new VideoViewModel(JsonConvert.DeserializeObject<List<Video>>(content).ToList());

                return Json( new { result = result });
            }

            return RedirectToAction("AccessDenied", "Authorization");
        }

        [HttpGet]
        public async Task<ActionResult> RefreshToken()
        {
            var accessToken = await _httpClientService.RefreshTokens();
            return Content(accessToken);
        }
    }
}
