﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MvcClient4.Services
{
    public interface IHttpClientService
    {
        Task<HttpClient> GetClient();
        Task<string> RefreshTokens();
        HttpClient CreateClient(string accessToken);
    }
}
