﻿using API2.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace API2
{
    public static class SeedData
    {
        public static void EnsureSeedDataForContext(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetService<VideoContext>();
                var videos = new List<Video>
                {
                    new Video
                    {
                        Id = new Guid("25320c5e-f58a-4b1f-b63a-8ee07a840bdf"),
                        Title = "An image by Annie",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    },
                    new Video
                    {
                        Id = new Guid("1efe7a31-8dcc-4ff0-9b2d-5f148e2989cc"),
                        Title = "An image by Loki",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    },
                    new Video
                    {
                        Id = new Guid("b24e3df5-0394-468d-9c1d-db1252fea920"),
                        Title = "An image by Nancy",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    },
                    new Video
                    {
                        Id = new Guid("9f35e705-637a-4bbe-8c35-402b2ecf7128"),
                        Title = "An image by Jennie",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    },
                    new Video
                    {
                        Id = new Guid("939df3fd-de57-4caf-96dc-c5e110322a96"),
                        Title = "An image by Loki",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    },
                    new Video
                    {
                        Id = new Guid("d70f656d-75a7-45fc-b385-e4daa834e6a8"),
                        Title = "An image by Loki",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    },
                    new Video
                    {
                        Id = new Guid("ce1d2b1c-7869-4df5-9a32-2cbaca8c3234"),
                        Title = "An image by Loki",
                        Embed = "https://youtu.be/BHCU7CR_Unk",
                        OwnerId = "7b5b9bd1-dbf2-400c-8cd7-09fc5244c702"
                    }
                };
                context.Videos.AddRange(videos);
                context.SaveChanges();
            }
        }
    }
}
