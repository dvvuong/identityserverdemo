﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace API2.Controllers
{
    [Route("api/identity")]
    [Authorize]
    public class IdentityController : Controller
    {
        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return new JsonResult(from c in User.Claims select new { c.Type, c.Value });
        }
    }
}