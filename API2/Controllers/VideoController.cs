﻿using API2.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API2.Controllers
{
    [Produces("application/json")]
    [Route("api/videos")]
    //[Authorize]
    public class VideoController : Controller
    {
        private readonly IVideoRepository _videoRepository;

        public VideoController(IVideoRepository videoRepository)
        {
            _videoRepository = videoRepository;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetVideos()
        {
            var videos = _videoRepository.GetVideos();
            List<Video> result = new List<Video>();
            result.AddRange(videos.Select(x => new Video(x.Id, x.Title, x.Embed, x.OwnerId)));

            return Ok(result);
        }

        [HttpGet]
        [Route("{id:Guid}")]
        //[Authorize("Admin")]
        public IActionResult GetVideo(Guid id)
        {
            var video = _videoRepository.GetVideo(id);

            if (video == null)
            {
                return NotFound();
            }

            var result = new Video(video.Id, video.Title, video.Embed, video.OwnerId);

            return Ok(result);
        }

        [HttpPost]
        //[Authorize("Admin")]
        [Route("")]
        public IActionResult CreateVideo([FromBody] CreateOrUpdateVideo req)
        {
            if (req == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var video = new API2.Entities.Video();
            video.Id = Guid.NewGuid();
            video.Title = req.Title;
            video.Embed = req.Embed;
            var ownerId = User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            video.OwnerId = ownerId;

            _videoRepository.AddVideo(video);

            if (!_videoRepository.Save())
            {
                throw new Exception($"Adding an Video failed.");
            }

            var result = new Video(video.Id, video.Title, video.Embed, video.OwnerId);

            return Ok(result);
        }

        [HttpDelete]
        [Authorize("User")]
        [Route("{id:Guid}")]
        public IActionResult DeleteVideo(Guid id)
        {

            var video = _videoRepository.GetVideo(id);

            if (video == null)
            {
                return NotFound();
            }

            _videoRepository.DeleteVideo(video);

            if (!_videoRepository.Save())
            {
                throw new Exception($"Delete video with {id} failed.");
            }

            return NoContent();
        }

        [HttpPut]
        [Authorize("User")]
        [Route("{id:Guid}")]
        public IActionResult UpdateVideo(Guid id,
            [FromBody] CreateOrUpdateVideo req)
        {

            if (req == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {

            }

            var video = _videoRepository.GetVideo(id);
            if (video == null)
            {
                return NotFound();
            }

            video.Title = req.Title;
            video.Embed = req.Embed;

            _videoRepository.UpdateVideo(video);

            if (!_videoRepository.Save())
            {
                throw new Exception($"Updating Video with {id} failed.");
            }

            return NoContent();
        }

    }
}