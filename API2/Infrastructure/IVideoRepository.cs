﻿using API2.Entities;
using System;
using System.Collections.Generic;

namespace API2.Infrastructure
{
    public interface IVideoRepository
    {
        IEnumerable<Video> GetVideos();
        IEnumerable<Video> GetVideosByOwner(string ownerId);
        bool IsVideoOwner(Guid id, string ownerId);
        Video GetVideo(Guid id);
        bool VideoExists(Guid id);
        void AddVideo(Video video);
        void UpdateVideo(Video video);
        void DeleteVideo(Video video);
        bool Save();
    }
}
