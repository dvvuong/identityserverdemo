﻿using API2.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace API2.Authorization
{
    public class ManageVideoHander : AuthorizationHandler<ManageVideoRequirement>
    {
        private readonly IVideoRepository _videoRepository;
        public ManageVideoHander(IVideoRepository videoRepository)
        {
            _videoRepository = videoRepository;
        }

        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context, ManageVideoRequirement requirement)
        {
            var filterContext = context.Resource as AuthorizationFilterContext;
            if (filterContext == null)
            {
                context.Fail();
                return Task.FromResult(0);
            }

            var videoId = filterContext.RouteData.Values["id"].ToString();
            //do something
            return Task.FromResult(0);
        }
    }
}
