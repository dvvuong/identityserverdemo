﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class CreateOrUpdateVideo
    {
        [Required]
        [MaxLength(150)]
        public string Title { get; set; }
        [Required]
        [MaxLength(4000)]
        public string Embed { get; set; }
        public string OwnerId { get; set; }
        public Guid Id { get; set; }
    }
}
