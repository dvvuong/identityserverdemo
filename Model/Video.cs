﻿using System;

namespace Model
{
    public class Video
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Embed { get; set; }
        public string EmbedCode { get; set; }
        public string OwnerId { get; set; }

        public Video(Guid id, string title, string embed, string ownerId = null)
        {
            Id = id;
            Title = title;
            Embed = embed;
            OwnerId = ownerId;
            EmbedCode = Embed.Substring(15);
        }
    }
}
