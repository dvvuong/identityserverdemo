﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MvcClient5.Services
{
    public interface IHttpClientService
    {
        Task<HttpClient> GetClient();
        Task<string> RefreshTokens();
        HttpClient CreateClient(string accessToken);
    }
}
