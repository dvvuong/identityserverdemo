﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MvcClient.Services
{
    public interface IHttpClientService
    {
        Task<HttpClient> GetClient();
    }
}
