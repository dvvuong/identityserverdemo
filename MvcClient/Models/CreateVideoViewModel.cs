﻿using System.ComponentModel.DataAnnotations;

namespace MvcClient.Models
{
    public class CreateVideoViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Embed { get; set; }
    }
}
