﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcClient.Models
{
    public class EditVideoViewModel
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public string Embed { get; set; }
        [Required]
        public Guid Id { get; set; }
    }
}
