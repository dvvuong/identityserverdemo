﻿using Model;
using System.Collections.Generic;

namespace MvcClient.Models
{
    public class VideoViewModel
    {
        public IEnumerable<Video> Videos { get; private set; }
            = new List<Video>();

        public VideoViewModel(List<Video> videos)
        {
            Videos = videos;
        }
    }
}
