﻿using Model;

namespace MvcClient.Models
{
    public class DetailVideoViewModel
    {
        public Video Video { get; private set; }

        public DetailVideoViewModel(Video video)
        {
            Video = video;
        }
    }
}
