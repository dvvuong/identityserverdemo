﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Model;
using MvcClient.Models;
using MvcClient.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MvcClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpClientService _httpClientService;

        public HomeController(IHttpClientService httpClientService)
        {
            _httpClientService = httpClientService;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var clients = await _httpClientService.GetClient();

            var response = await clients.GetAsync("api/videos");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var result = new VideoViewModel(JsonConvert.DeserializeObject<List<Video>>(content).ToList());

                return View(result);
            }

            return RedirectToAction("AccessDenied", "Authorization");
        }

        [Authorize]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> Create(CreateVideoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var videoCreation = new CreateOrUpdateVideo
            {
                Title = model.Title,
                Embed = model.Embed,
                OwnerId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "sub").Value
            };

            var serializedVideoCreation = JsonConvert.SerializeObject(videoCreation);
            var clients = await _httpClientService.GetClient();

            var response = await clients.PostAsync("api/videos", new StringContent(serializedVideoCreation, Encoding.Unicode, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [Authorize]
        [Authorize(Policy = "CanEdit")]
        public async Task<IActionResult> Edit(Guid id)
        {
            var clients = await _httpClientService.GetClient();
            var response = await clients.GetAsync($"api/videos/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var deserializeObject = JsonConvert.DeserializeObject<Video>(content);

                var editViewModel = new EditVideoViewModel
                {
                    Id = deserializeObject.Id,
                    Title = deserializeObject.Title,
                    Embed = deserializeObject.Embed
                };

                return View(editViewModel);
            }

            return RedirectToAction("AccessDenied", "Authorization");
        }

        [Authorize]
        [Authorize(Policy = "CanEdit")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Edit(EditVideoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var videoCreation = new CreateOrUpdateVideo
            {
                Title = model.Title,
                Embed = model.Embed,
                Id = model.Id
            };

            var serializedVideoEdit = JsonConvert.SerializeObject(videoCreation);
            var clients = await _httpClientService.GetClient();

            var response = await clients.PutAsync($"api/video/{videoCreation.Id}", new StringContent(serializedVideoEdit, Encoding.Unicode, "application/json"));

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("AccessDenied", "Authorization");
        }

        [Authorize]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var clients = await _httpClientService.GetClient();
            var response = await clients.DeleteAsync($"api/videos/{id}");

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("AccessDenied", "Authorization");
        }

        [Authorize]
        public async Task<IActionResult> View(Guid id)
        {
            var clients = await _httpClientService.GetClient();
            var response = await clients.GetAsync($"api/videos/{id}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var deserializeObject = JsonConvert.DeserializeObject<Video>(content);

                var editViewModel = new DetailVideoViewModel(new Video(deserializeObject.Id, deserializeObject.Title, deserializeObject.Embed));

                return View(editViewModel);
            }

            return RedirectToAction("AccessDenied", "Authorization");
        }

        [Authorize]
        public IActionResult Test()
        {
            ViewData["Message"] = "Test Client page.";

            return View();
        }

        public async Task Logout()
        {
            await HttpContext.SignOutAsync("Cookies");
            await HttpContext.SignOutAsync("oidc");
        }

        public IActionResult Error()
        {
            return View();
        }

        public async Task<IActionResult> CallApi()
        {
            var accessToken = await HttpContext.GetTokenAsync("access_token");

            var client = new HttpClient();
            client.SetBearerToken(accessToken);
            var content = await client.GetStringAsync("http://localhost:44388/api/values");

            ViewBag.Json = JArray.Parse(content).ToString();
            return View("json");
        }
    }
}
